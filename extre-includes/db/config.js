const mongoose = require('mongoose')
const Schema = mongoose.Schema

const mongoConnect = (mongourl) => {
  return mongoose.createConnection(
  mongourl, 
  {
    autoIndex: false,
    reconnectTries: 30,
    reconnectInterval: 500,
    poolSize: 10,
    bufferMaxEntries: 0,
    useNewUrlParser: true
  })
}

const pluginSchema = new Schema({
  isEnabled: Boolean,
  name: String,
  mongourl: { type: String, default: '' }
})

const configSchema = new Schema({
  name: { type: String, default: 'extre-config' },
  siteTitle: String,
  siteDescription: String,
  plugins: pluginSchema,
  theme: {
    admin: {
      name: String,
      mongourl: { type: String, default: '' }
    },
    client: {
      name: String,
      mongourl: { type: String, default: ''}
    }
  },
  registrationEnabled: Boolean,
  oauthEnabled: Boolean,
  allowUserThemes: Boolean,
})

module.exports = class Config {
  constructor (url) {
    this._connection = mongoConnect(url)
    this._config = this._connection.model('configs', configSchema)
  }

  async setConfig(query) {
    await this._config.findOneAndUpdate({ name: 'extre-config' }, { $set: query })
    return 'Great Success!'
  }

  async togglePlugin(name) {
    let query = await this._config.findOne({ name: 'extre-config' })
    for(var plugin in query.plugins) {
      if(query.plugins[plugin].name === name) {
        query.plugins[plugin].isEnabled = !query.plugins[plugin].isEnabled
        await query.save()
        break
      }
    }
  }

  async changePluginDB(name, url) {
    let query = await this._config.findOne({ name: 'extre-config' })
    for(var plugin in query.plugins) {
      if(query.plugins[plugin].name === name) {
        query.plugins[plugin].mongourl = url
        await query.save()
        break
      }
    }
  }

  async changeTheme(type, name) {
    let query = await this._config.findOne({ name: 'extre-config' })
    query.theme[type].name = name
    await query.save()
  }

  async changeThemeDB(type, url) {
    let query = await this._config.findOne({ name: 'extre-config' })
    query.theme[type].mongourl = url
    await query.save()
  }

  async getConfig() {
    return await this._config.findOne({ name: 'extre-config' })
  }
}