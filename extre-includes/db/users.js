const mongoose = require('mongoose')
const Schema = mongoose.Schema
const moment = require('moment')
const bcrypt = require('bcrypt')
const salt = 10

const checkID = (id) => {
  try {
    moment(parseInt(`0x${id.substr(0, 8)}`))
    return true
  } catch(err) {
    return false
  }
}

module.exports = class Config {
  constructor (connect, schema) {
    this._connection = connect()
    this._userSchema = new Schema(schema)
    this.users = this.connection.model('users', this._userSchema)
  }

  async updateField(id, update) {
    if(checkID(id)) {
      if(update.password) await this.users.findByIdAndUpdate(id, { $set: { password: bcrypt.hashSync(update.password, salt)}})
      else await this.users.findByIdAndUpdate(id, { $set: update })
    }
    else {
      if(update.password) await this.users.findOneAndUpdate({ $or: [{ username: id }, { email: id }] }, { $set: { password: bcrypt.hashSync(update.password, salt)}})
      else await this.users.findOneAndUpdate({ $or: [{ username: id }, { email: id }] }, { $set: update })
    }
  }

  async findUser(id) {
    if(checkID(id)) return this.users.findById(id)
    else return this.users.findOne({ $or: [{ username: id }, { email: id }]})
  }

  async createUser(user) {
    let user = new this.users(user)
    await user.save()
  }
  
  async getUsers(filter, sort) {
    return this.users.find(filter, sort)
  }
}