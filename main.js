const fs = require('fs')
const CONFIG = {
  port: process.env.PORT || 3000,
  host: process.env.SERVER_HOST || 'localhost',
  cookiePass: process.env.COOKIE_PASS || '12345678909876543211234567890',
  mongourl: process.env.MONGO_URL || `mongodb://${process.env.SERVER_HOST || 'localhost'}:27017/extre`
}
//  Init servers
const { Server } = require('hapi')
const server = new Server({
  port: CONFIG.port || 3000,
  host: CONFIG.host || 'localhost'
})
const mongoose = require('mongoose')

//  Connect DBs

const ConfigDB = require('./extre-includes/db/config')
const configdb = new ConfigDB(CONFIG.mongourl)

const restartServer = async () => {
  server.stop()
  process.on("exit", () => {
    require('child_process').spawn(process.argv.shift(), process.argv, {
      cwd: process.cwd(),
      detached: true,
      stdio: 'inherit'
    }).unref()
  })
  setTimeout(() => {
    process.exit(0)
  }, 5000)
}

const startServer = async () => {
  await server.register( require('inert') )
  let config = await configdb.getConfig()
  try {
    for(var plugin in config.plugins) {
      if(config.plugins[plugin].isEnabled) server.register( `./extre-content/${config.plugins[plugin].name}/index.js` )
    }
  } catch(err) {
    let newConfig = new configdb._config({
      name: 'extre-config',
      siteTitle: 'Extre site',
      siteDescription: 'Read all about it!',
      theme: {
        admin: {
          name: 'default',
        },
        client: {
          name: 'default',
        }
      },
      registrationEnabled: true,
      oauthEnabled: true,
      allowUserThemes: false,
    })
    newConfig.save()
  }

  server.route({
    method: 'GET',
    path: '/admin/{file?}',
    options: {

    },
    handler: async (request, h) => {
      return h.file('')
    }
  })

  server.route({
    method: 'GET',
    path: '/api/version',
    handler: (request, h) => {
      return require('./package.json').version
    }
  })
}

startServer()
console.log(`Server started at ${server.settings.host}:${server.settings.port}`)